//1. if-else
// Membuat Program untuk Mengecek input dari Pemain dan Hasil Response dari Game sesuai input yang Dikirimkan.
var nama = "Junaedi"
var Peran = "Warewolf"
if (nama == ""){
    console.log("Nama harus diisi")
} else if (nama == "John" || Peran == ""){
    console.log("Halo John, pilih peranmu untuk memulai game!")
} else if (nama == "Jane" || "Penyihir"){
    console.log("Selamat datang di Dunia Werewolf, Jane")
    console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf")
} else if (nama == "Jenita" || Peran == "Guard"){
    console.log("Selamat datang di Dunia werewolf, Jenita")
    console.log("Hallo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf")
} else if (nama == " Junaedi" || Peran == "Werewolf"){
    console.log("Selamat datang di Dunia Werewolf, Junaedi")
    console.log("Halo Werewolf Junaedi, kamu akan memakan mangsa setiap malam!")
} else {
    console.log("Game berakhir!!")
}

//2. Switch Case
var hari = 20
var bulan = 1
var tahun = 2000

if(hari <= 31 || bulan <= 12 || tahun <= 1900 || tahun >= 2200){
    switch (bulan){
        case 1 : bulan = 'Januari'; break;
        case 2 : bulan = 'Februari'; break;
        case 3 : bulan = 'Maret'; break;
        case 4 : bulan = 'April'; break;
        case 5 : bulan = 'Mei'; break;
        case 6 : bulan = 'Juni'; break;
        case 7 : bulan = 'Juli'; break;
        case 8 : bulan = 'Agustus'; break;
        case 9 : bulan = 'September'; break;
        case 10 : bulan = 'Oktober'; break;
        case 11 : bulan = 'November'; break;
        break;
        default :
        bulan = ('Tidak ada bulan')
    }
    console.log(hari+ ' '+bulan+' '+tahun)
}