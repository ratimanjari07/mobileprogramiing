//A. Looping While
{
console.log('Looping Pertama')
var loop = 2;
while(loop <= 20) {
    if (loop %2==0) {
        console.log(loop + ' - I Love Coding');
    }
    loop++
}
console.log('Looping Kedua')
while(loop >= 2){
    if (loop %2==0) {
        console.log(loop + ' - I will become fullstack javascript developer');
    }
    loop--
}
}

//B. Looping For
console.log('\n');
{
    for (var i=1;i<=20;i++){
        if (i%2==0){
            console.log(i+ " - Informatika")
        } else if (i%3==0){
            console.log(i+ " - I Love Coding")
        }else {
            console.log(i+ " - Teknik")
        }
    }
}


//C. Membuat Persegi Panjang #
console.log('\n');
{
    for ( a = 0; a <= 8; a++){
            console.log("####")
        }
}


//D. Membuat Tangga
console.log('\n');
{
    var sisi = "";
    for (a = 0; a <= 7; a++){
        for ( b = 0; b < a; b++){
            sisi += "#";
        }
        sisi +="\n";
    }
    console.log(sisi);
}